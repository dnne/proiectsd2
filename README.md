# Proiect SD 2:GeoMarketing
# **Software aplication for SportRUs**  


  
## **Build** and **run**    

1.`git clone https://gitlab.com/dnne/proiectsd2.git`   
2.`cd proiectsd2/checker`   
3.`make debug`   
4.`./a.out < inputGenerator/<ref_file>`   

## **Generate map:**   
1. `cd proiectsd2/Harta`   
2. `./generatorHarta.sh <ref_file>`   
3. Open Hartagenerata.html file with your browser!  

Where <ref_file> should be:    
ref_file_10k_10k.txt  
ref_file_1k_1k.txt  
  
  
  
## **Team 311CAa**
+ Cristian Done 
+ Cristian Duţescu
+ Radu Nicolau
+ Daniel Dosaru
+ Cristina Buciu
+ Daniel Bălteanu
+ Camelia Moise
+ Claudiu Nedelcu
+ Irina Băeţica
+ Sergiu Isopescu
+ Teodora Chiriac
+ Radu Ciunghia
+ Antonia Nicolaescu

Coach: Andrei Vasiliu
