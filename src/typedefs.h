#ifndef TYPEDEFS_H
#define TYPEDEFS_H

typedef int UserID;
typedef UserID GroupID;
typedef int StoreID;
typedef int StoreGroupID;
typedef int Time;
typedef int Discount;
typedef int Visits;
typedef int Invites;
typedef int DayIndex;
typedef double Distance;
typedef long long StoreAndUserIDS;

#endif