/*
Service - clasa ceruta a fi implementata
*/

#ifndef SERVICE_H
#define SERVICE_H

#include <cmath>
#include <iostream>
#include "other/invite_graph.h"
#include "generic_data_structures/unordered_set.h"
#include "other/stores.h"
#include "other/store_group.h"
using namespace std;

class Service {
private:

    // Utilizatorii
    UsersManager users;

    // Magazinele
    StoresMap stores;

    // Informatiile despre grupuri
    InviteGraph invite_graph;

    // Hash Table cu cheia id-ul magazinului si valoarea un arbore
    // Arborele corespunzator fiecarui magazin are drept cheia un moment de timp
    // si valoare o pereche
    // Perechea descrisa anterior are primul camp discountul total pentru acel moment de timp
    // iar cel de-al doilea numarul total de vizite corespunzatoare acelui moment de timp
    // Pair::t1 = discounts
    // Pair::t2 = visits
    HashMap<StoreID, AvlTree<Time, Pair<Discount,Visits> > > discounts_and_visits;

    // Topul discounturilor pentru fiecare magazin
    // Hash Table cu cheia id-ul magazinului si valoarea un arbore
    // Arborele corespunzator fiecarui magazin are drept cheia un Discount
    // si valoare numarul de discounturi
    HashMap<StoreID, AvlTree<Discount,int> > top_discounts;

    // Topul distantelor de la care vin utilizatorii unui magazin
    // Hash Table cu cheia id-ul magazinului si valoarea un arbore
    // Arborele retine un set de distante
    HashMap<StoreID, AvlSet<Distance> >top_distances;

    // Numarul de vizite dintr-o zi pentru fiecare magazin
    // Hash Table cu cheia id-ul magazinului si valoarea un pointer catre un alt hashtable
    // Pointer-ul arata catre un Hash Table care are cheie o zi si valaore numarul de vizite din
    // aceea zi
    HashMap<StoreID, HashMap<DayIndex, Visits>* > visits_in_day;

    // Propunerile pentru deschiderea unui nou magazin
    // Pointer catre un Hash Table cu cheia id-ul unui grup de persoane care ar beneficia de
    // de un nou magazin intr-o anumita locatie si valoarea o structura in care sunt
    // stocate informatii despre acest grup.
    // Grupul de persoane care ar beneficia de magazin nu are nicio legatura cu grupurile
    // de utilizatori invitati
    HashMap<int,StoreGroupData> *store_groups;

    // Id-ul urmatorului grup de persoanre care ar beneficia de un nou magazin intr-o anumita 
    // locatie
    StoreGroupID next_store_group_id;
    

    // Functia de hash pentru StoreID
    static unsigned int hashStoreID(StoreID id, unsigned buckets)
    {
        return id%buckets;
    }

    // Functia de hash pentru it-ul zilei
    static unsigned int hashDayIndex(DayIndex day, unsigned buckets)
    {
        return day%buckets;
    }

    // Functia de comparare a doua zile - folosita pentru adaugare in lista
    static int compare_days(const DayIndex& day1, const DayIndex& day2)
    {
        if(day1 < day2)
            return -1;
        if(day1 > day2)
            return 1;
        return 0;
    }

     // Functie de comparare pentru raportul de la usersWithBestBuyToDiscountRate
    static int compare_fraction(const Pair<int,double>& a , const Pair<int,double>& b)
    {
        if(a.t2 < b.t2)
            return -1;
        if(a.t2 > b.t2)
            return 1;
        if(a.t2 == b.t2)
        {
            if (a.t1 < b.t1)
                return 1;
            if (a.t1 > b.t1)
                return -1;
            return 0;
        }

    }

    // Adaugarea unui utilizator intr-un grup de persoane care ar beneficia de 
    // un nou magazin intr-o anumita zona
    void addUserToStoreGroup(GlobalPosition home, Distance distance_to_closest_shop)
    {
        int closest_group_id = INVALID_STORE_GROUP;
        Distance closest_group_dist = std::numeric_limits<Distance>::infinity();
        Distance crt_dist;

        // Parcugerea tuturor grupurilor de persoane care ar beneficia de un nou magazin
        // intr-o anumita zona
        for(HashMap<int,StoreGroupData>::Iterator 
            groups_it = store_groups->begin() ; groups_it.isValid() ; ++groups_it)
        {
            // Daca sunt indeplinite conditiile pentru ca utilizatorul sa poata fi
            // adaugat la grupul curent si daca magazinul propus este mai aproape se
            // retine ca fiind cea mai buna varianta pentru utilizatorul curent
            // (caracterizat prin locatia home si distanta distance_to_closest_shop catre 
            // cel mai apropiat mazagin deja existent)
            // Pozitia magazinului pentru un grup de utilizatori dintr-o zona este calulata ca
            // medie aritmetica a corrdonatelor caselor acestora
            // Pentru ca un utilizator sa poate fi adaugat intr-un grup de persoane dintr-o zona
            // acesta trebuie ca daca daca ar fi adaugat in grup sa fie mai aproape de acest posibil
            // magazin decat fata de cel mai apropiat de el in momentul actual si sa nu se modifice
            // mult coordonatele magazinului (distanta catre magazinul propus sa ramana mai mica decat
            // distanta catre cel mai apropiat magazin actual)
            if(groups_it.getValue().isGoodStage1(
                    home,
                    distance_to_closest_shop,
                    crt_dist)
                && crt_dist < closest_group_dist
                && groups_it.getValue().isGoodStage2(home))
            {
                closest_group_id = groups_it.getKey();
                closest_group_dist = crt_dist;
            }
        }

        // Daca a fost gasit un grup de persoane dintr-o zona in care poate fi adaugat utilizatorul
        // atunci este adaugat in cel care propune ca noul magazin sa fie cat mai aproape
        if(closest_group_id != INVALID_STORE_GROUP)
        {
            store_groups->getValue(closest_group_id).add(
                home,
                distance_to_closest_shop);
        }
        // Nu s-a gasit un grup. Se creaza unul in care va fi doar utilizatorul curent (momentan)
        else
        {
            store_groups->add(
                next_store_group_id++,
                StoreGroupData(
                    home, 
                    distance_to_closest_shop)
                );
        }
    }



public:

    // Constructor

    Service(): users(), invite_graph(&users, UsersManager::hashUserID),
        discounts_and_visits(STORES_BUCKETS, Service::hashStoreID),
        top_discounts(STORES_BUCKETS, Service::hashStoreID),
        top_distances(STORES_BUCKETS, Service::hashStoreID),
        visits_in_day(STORES_BUCKETS, Service::hashStoreID)
    {
        store_groups = nullptr;
        next_store_group_id = 0;
    }

    // Destructor

    ~Service()
    {
        // HashMap<StoreID, HashMap<DayIndex, Visits>* > visits_in_day;
        for(HashMap<StoreID, HashMap<DayIndex, Visits>* >::Iterator
            it = visits_in_day.begin() ; it.isValid() ; ++it)
        {
            delete it.getValue();
        }

        if(store_groups != nullptr)
            delete store_groups;
    }

    // Createa unui utlizator

    void createUser(int id, double homeX, double homeY)
    {
        // Memorarea coordonatelor casei
        UserData data;
        data.home.latitude  = homeX;
        data.home.longitude = homeY;

        // Calcularea distantei catre cel mai apropiat magazin
        Distance crt_dist;
        for(HashMap<StoreID, StoreData>::Iterator it=stores.stores.begin() ; it.isValid() ; ++it)
        {
            crt_dist = data.home.DistanceTo(it.getValue().location);
            if(crt_dist < data.distance_to_closest_shop)
                data.distance_to_closest_shop = crt_dist;
        }

        // Adaugarea utilizatorului in structura corespunzatoare
        users.addUser(id, data);

        // Adaugarea unui componente conexe pentru utilizator in graful invitatiilor
        invite_graph.addUser(id);

        // Daca au mai fost distribuiti utilizatorii pe zone (pentru deschidearea unui nou magazin)
        // si nu au mai fost adaugate magazine se incadreaza noul utilizator intr-un grup dintr-o 
        // anumita zona
        if(store_groups != nullptr)
        {
            addUserToStoreGroup(data.home,data.distance_to_closest_shop);
        }
    }

    void createStore(int id, double storeX, double storeY) 
    {
        StoreData data;
        data.location.latitude=storeX;
        data.location.longitude=storeY;

        // Actuzalizarea celui mai apropiat magazin pentru fiecare utilizator
        Distance crt_dist;
        for(HashMap<UserID, UserData>::Iterator it=users.users.begin() ; it.isValid() ; ++it)
        {
            crt_dist = data.location.DistanceTo(it.getValue().home);
            if(crt_dist < it.getValue().distance_to_closest_shop)
                it.getValue().distance_to_closest_shop = crt_dist;
        }

        stores.addStore(id,data);

        // Propunerile pentru un nou magazin nu mai sunt valabile
        if(store_groups != nullptr)
        {
            delete store_groups;
            store_groups = nullptr;
        }
    }

    void visit(int timestamp, int clientId, int storeId, int discount)
    {
        // Actuzalizarea vizitelor pentru componenta conexa corespunzatoare
        invite_graph.visit(clientId);

        UserData &user =  users.getUserData(clientId);

        StoreData &store = stores.getStoresData(storeId);

        if(discount != -1)
        {
            // Actualizarea numarului de cumparaturi(vizite in care a primit discount)
            // si a sicountului primit de acel utilizator
            user.total_discounts += discount;
            user.buys++;

            AvlTree<Discount,int> *discount_tree;

            // Adaugare in topul discount-urilor coresppunzatoare acelui magazin
            try{
                discount_tree = &top_discounts.getValue(storeId);
            }
            catch(...)
            {
                top_discounts.add(storeId,AvlTree<Discount,int>());
                discount_tree = &top_discounts.getValue(storeId);
            }

            try{
                discount_tree->getValue(discount)++;
            }
            catch(...)
            {
                discount_tree->set(discount,1);
            }

        }

        AvlTree<Time, Pair<Discount,Visits> > *store_tree;

        // Actuzaliarea arborelui pentru discount-uri si vizite corespunzatoare acelui magazin
        // la un anumit moment de timp
        try{
            store_tree = & discounts_and_visits.getValue(storeId);
        }
        catch(...)
        {

            discounts_and_visits.add(storeId, AvlTree<Time, Pair<Discount, Visits> >() );
            store_tree = & discounts_and_visits.getValue(storeId);
        }

        try{
            Pair<Discount,Visits> *pair;
            pair = &store_tree->getValue(timestamp);
            if(discount != -1)
                pair->t1 += discount;
            pair->t2 += 1;    
        }
        catch(...)
        {
            Pair<Discount,Visits> pair;
            if(discount != -1)
                pair.t1 = discount;
            else
                pair.t1 = 0;
            pair.t2 = 1;
            store_tree->set(timestamp, pair);
        }

        // Actuzalizarea topului celor mai mari distante de la care 
        // vin utilizatorii pentru magazinul curent
        Distance distance;
        distance = user.home.DistanceTo(store.location);

        AvlSet<Distance> *distance_tree;

        try{
            distance_tree = &top_distances.getValue(storeId);
        }
        catch(...)
        {
            top_distances.add(storeId,AvlSet<Distance>());
            distance_tree = &top_distances.getValue(storeId);
        }

        distance_tree->set(distance);

        // Addaugare in visits_in_day
        DayIndex day= timestamp / (24 * 3600);

        HashMap<DayIndex, Visits> *visits;

        try{
            visits = visits_in_day.getValue(storeId);
        }
        catch(...)
        {
            visits_in_day.add(storeId, 
                new HashMap<DayIndex, Visits>(DAYS_BUCKETS,Service::hashDayIndex) );
            visits = visits_in_day.getValue(storeId);
        }

        try{
            visits->getValue(day)++;
        }
        catch(...)
        {
            visits->add(day,1);
        }

    }

    void invite(int userWhichInvites, int invitedUser)
    {
        if(userWhichInvites == invitedUser)
            return;

        // Memorarea utilizatorului de catre care a fost invitat
        users.getUserData(invitedUser).invited_by = userWhichInvites;

        // Actualizarea componentelor conexe
        invite_graph.invite(userWhichInvites, invitedUser);
    }

    // numar de vizite in intervalul (startTime, endTime)
    int visitsInTimeframe(int startTime, int endTime)
    {
        int visits = 0;

        // Parcurgerea tuturor magazinelor
        for(HashMap<StoreID, AvlTree<Time, Pair<Discount,Visits> > >::Iterator 
            hash_it=discounts_and_visits.begin(); hash_it.isValid() ; ++hash_it)
        {
            // Parcurgerea arborelui de discount si vizite corespunzator magazinului
            // curent pornind de la startTime pana la endTime
            for(AvlTree<Time, Pair<Discount,Visits> >::Iterator 
                tree_it = hash_it.getValue().getNodeAfterKey(startTime); 
                tree_it.isValid() && tree_it.getKey()<=endTime ; ++tree_it)
            {
                // Actualizarea numarului de vizite din interval
                visits+=tree_it.getValue().t2;
            }
        }
        return visits;
    }

    // Discount-ul total in intervalul (startTime, endTime)
    int totalDiscountInTimeframe(int startTime, int endTime)
    {
        int discounts = 0;
        // Parcurgerea magazinelor
        for(HashMap<StoreID, AvlTree<Time, Pair<Discount,Visits> > >::Iterator 
            hash_it=discounts_and_visits.begin(); hash_it.isValid() ; ++hash_it)
        {
            // Parcurgerea arborelui de discount si vizite pentru magazinul curent
            // pornind de la startTime pana la endTime
            for(AvlTree<Time, Pair<Discount,Visits> >::Iterator 
                tree_it = hash_it.getValue().getNodeAfterKey(startTime); 
                tree_it.isValid() && tree_it.getKey()<=endTime ; ++tree_it)
            {
                discounts+=tree_it.getValue().t1;
            }
        }
        return discounts;
    }

    // Returneaza un Array de k elemente cu id-urile user-ilor
    // Buy to discount rate se calculeaza in felul urmator
    // buy = numarul de vizite in care user-ul a cumparat ceva (discount > 0)
    // total = discount-ul total pe care utilizatorul la primit (excluzand vizitele cu discount = -1)
    // buyToDiscountRate = buy / total
    Array<int> usersWithBestBuyToDiscountRate(int K) {
        int* results = nullptr;
        if(K <= 0)
            return Array<int>(0, results); 

        results = new int[K]; 
        int limit = 0;

        try{

            // Lista cu topul celor mai bune K raporturi
            SinglyLinkedList<Pair<int, double>> top;

            // Numarul curent de elemente in lista topurilor
            int list_size = 0;

            Pair<int, double> data;//pereche de id şi raport
            
            // Parcurgerea tuturor utilizatorilor
            for(HashMap<UserID, UserData> ::Iterator 
            hash_it=users.users.begin(); hash_it.isValid(); ++hash_it)
            {   
                int disc = hash_it.getValue().total_discounts;
                int nr_buys = hash_it.getValue().buys;

                // Selectarea utilizatorilor care au cumparat cel putin o data
                if (nr_buys > 0)
                {
                    // Calularea raportului
                    double r = (double) nr_buys/disc;
                    
                    // Realizarea perechii pentru adaugarea in top
                    data.t1 = hash_it.getKey();//id
                    data.t2 = r;//raport

                    // Daca lista nu este plina se adauga
                    if (list_size < K)
                    {
                        list_size++;
                        top.addSorted(data, compare_fraction);
                    }

                    // Daca lista este plina
                    else
                    {
                        // Se face verificarea daca noul raport e mai mare
                        // decât cel mai mic raport din lista sau este egal dar id-ul e mai mic
                        if (compare_fraction(data,top.getFirst()) > 0) 
                        {
                            // Se scoate cel mai nesemnificativ raport din lista 
                            top.removeFirst();
                            // Se adauga noul raport in lista
                            top.addSorted(data, compare_fraction);
                        }
                    }
                }
            }

            // Se parcurge lista topurilor
            for(SinglyLinkedList<Pair<UserID, double>> ::Iterator 
            list_it=top.begin(); list_it.isValid(); ++list_it)
            {
                // Se adauga in Array doar id-urile utilizatorilor
                results[limit++] = list_it.getValue().t1;
            }

        }
        catch(...){}

        return Array<int>(limit, results); 
    }


    // Numar de vizite in intervalul (startTime, endTime) pentru magazinul storeId
    int visitsInTimeframeOfStore(int startTime, int endTime, int storeId)
    {
        int visits = 0;
        try{
            // Daca exista un arbore de discounturi si vizite pentru magazinul ales
            // se parcurge pornind de la startTime pana la endTime
            for(AvlTree<Time, Pair<Discount,Visits> >::Iterator 
                tree_it = discounts_and_visits.getValue(storeId).getNodeAfterKey(startTime); 
                tree_it.isValid() && tree_it.getKey()<=endTime ; ++tree_it)
            {
                // Se actualizeaza numarul de vizite
                visits+=tree_it.getValue().t2;
            }   
        }
        catch(...)
        {}
        return visits;
    }

    // Cele mai mari discount-uri acordate in magazinul storeId
    Array<int> biggestKDiscounts(int K, int storeId)
    {
        if(K <= 0)
            return Array<int>(0, nullptr);    

        int crt_pos_v = 0;
        int i;
        int *v = new int[K];

        try{
            // Parcurgerea topului de discounturi corespunzatoarea magazinului ales (daca exista)
            for(AvlTree<Discount,int>::Iterator
                it = top_discounts.getValue(storeId).end() ; it.isValid() && crt_pos_v < K ; --it)
            {
                // Se adauga discount-ul curent in Array de numarul de ori de care apare
                for(i=0 ; i< it.getValue() && crt_pos_v < K ; i++)
                    v[crt_pos_v++] = it.getKey();
            }
        }
        catch(...){}

        return Array<int>(crt_pos_v, v);
    }


    // Cele mai mari distante de la care vin clientii la magazinul storeId
    // Returneaza primele k distante dintre magazine si clienti
    // Foloseste arborele AVL creat in visites si afiseaza cele mai mari valori 
    Array<double> biggestKClientDistances(int K, int storeId) {
       if(K <= 0)
            return Array<double>(0, nullptr);    

        // Numarul curent de elemente din Array
        int crt_pos_v = 0;
        double *v = new double[K];

        try{
            // Parcurgerea topului de distante
            for(AvlSet<Distance>::Iterator
                it = top_distances.getValue(storeId).end() ; it.isValid() && crt_pos_v < K ; --it)
            {
                // Adaugarea in Array
                v[crt_pos_v++] = it.getKey();
            }
        }
        catch(...){}

        return Array<double>(crt_pos_v, v);
    }

    // Returneaza un array cu index-ul zilelor cele mai aglomerate
    // Index-ul unei zile este timestamp / (24 * 3600)
    // In cazul in care 2 zile au acelasi numar de vizite 
    // in output va trebuie sa apara prima cea care are indicele mai mic
    Array<int> mostCrowdedKDays(int K, int storeId) 
    {
        if(K <= 0)
            return Array<int>(0, nullptr);

        // Topul celor mai aglomerate zile
        AvlTree<Visits,SinglyLinkedList<DayIndex> > top;

        SinglyLinkedList<DayIndex> *days_list;

        HashMap<DayIndex, Visits> *visits;

        // Obtinearea Hash Table-ului pentru magazinul ales
        try{
            visits = visits_in_day.getValue(storeId);
        }
        catch(...)
        {
            return Array<int>(0, nullptr);
        }

        int *v = new int[K];

        // Parcurgerea tuturor zilelor
        for(HashMap<DayIndex, Visits>::Iterator it = visits->begin() ;
            it.isValid() ; ++it)
        {
            // Adaugarea in topul celor mai aglomerate zile
            try{
                days_list = &top.getValue(it.getValue());
            }
            catch(...)
            {
                top.set(it.getValue(),SinglyLinkedList<DayIndex>());
                days_list = &top.getValue(it.getValue());
            }

            days_list->addSorted(it.getKey(), Service::compare_days);
        }

        SinglyLinkedList<DayIndex>::Iterator day_it;
        int days = 0;

        // Parcurgerea topului
        for(AvlTree<Visits,SinglyLinkedList<DayIndex> >::Iterator it = top.end() ; 
            it.isValid() && days < K ; --it)
        {
            for(day_it = it.getValue().begin() ; day_it.isValid() && days < K ; ++day_it)
            {
                // Adaugare in array
                v[days++] = day_it.getValue();
            }
        }

        return Array<int>(days, v);
    };

    // Returneaza un array cu dimensiunile grupurilor de utilizatori
    // Un grup de utilizatori se poate forma DOAR pe baza invitatiilor
    // Cu alte cuvinte, orice grup va avea dimensiunea minim 2
    Array<int> distinctGroupsOfUsers() {
        return invite_graph.getConnectedComponentsSizes();
    }

    // Id-ul userului care a invitat cei mai multi utilizatori
    // Daca sunt doi cu aceeasi valoare, se va intoarce id-ul minim
    // Daca NU exista niciun utilizator adaugat si intalniti un astfel de query
    // returnati -1
    int userWithMostInvites() {
        return invite_graph.topInviter();
    }

    // Lungimea celui mai lung lant de invitatii
    // Lungimea lantului se masoara ca numar de invitatii
    // A invita B => Lungime 1
    // A invita B, B invita C => Lungime 2
    int longestInvitesChainSize() {
        return invite_graph.getMaxDepth();
    }

    // Id-urile grupurilor cu cele mai multe invitatii overall
    // Id-ul unui grup se considera minimul id-urilor utilizatorilor din acel grup
    Array<int> topKGroupsWithMostVisitsOverall(int K) {
        return invite_graph.topKVisitsGroup(K);
    }

    // O lista de perechi de forma (idGrup, numarMediuDeVizite)
    // Id-ul unui grup se considera minimul id-urilor utilizatorilor din acel grup
    Array<pair<int, double>> averageVisitsPerUser() 
    {
        Array<pair<int, double>> results;
        invite_graph.getVisitsPerUser(results);
        return results;
    };
    
    // Returneaza latitudine si longitudine pentru locatia recomandata pentru un nou magazin
    pair<double, double> newStoreCoordinates() 
    {
        StoreGroupData new_store;
        HashMap<int,StoreGroupData>::Iterator groups_it;

        HashMap<UserID, UserData>::Iterator users_it=users.users.begin();

        // Daca nu sunt utilizatori locatia este (0,0)
        if(!users_it.isValid())
            return pair<double, double>(0,0);

        // Daca nu este disponibila o distribuire- se realizeaza una noua
        if(store_groups == nullptr)
        {

            store_groups = new HashMap<int,StoreGroupData>(
                STORE_GROUPS_BUCKETS,
                StoreGroupData::hashStoreGroupID);

            next_store_group_id = 0;

            // Se creaza un grup pentru primul utilizator
            new_store = StoreGroupData(
                    users_it.getValue().home, 
                    users_it.getValue().distance_to_closest_shop);
                store_groups->add(
                    next_store_group_id++,
                    new_store
                    );

            // Se parcurg utilizatorii
            for(++users_it ; users_it.isValid() ; ++users_it)
            {
                // Se adauga intr-un grup corespunzator
                addUserToStoreGroup(users_it.getValue().home, users_it.getValue().distance_to_closest_shop);
            }      
        }
        

        // Se parcurg propunerile pentru un nou magazin
        int best_group_id = INVALID_STORE_GROUP;
        int best_nr_users = 0;
        for(groups_it = store_groups->begin() ; groups_it.isValid() ; ++groups_it)
        {
            // Se alege magazinul care va avea un numar cat mai mare de utilizatori care ar beneficia
            if(groups_it.getValue().nr_users > best_nr_users)
            {
                best_group_id = groups_it.getKey();
                best_nr_users = groups_it.getValue().nr_users;
            }
        }

        // Se calculeaza pozitia noului magazin
        GlobalPosition next_store = store_groups->getValue(best_group_id).sum;
        next_store /= store_groups->getValue(best_group_id).nr_users;

        return pair<double, double>(next_store.latitude,next_store.longitude);
    }
};

#endif
