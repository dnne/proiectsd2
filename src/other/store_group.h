/*
StoreGroupData - clasa folosita pentru pentu a memora informatiile necesare unui magazin propus
a fi nou
*/

#ifndef STORE_GROUP
#define STORE_GROUP

#include <limits>
#include "../generic_data_structures/singly_linked_list.h"
#include "global_position.h"
#include "../typedefs.h"

class StoreGroupData{
private:
	class UserDistData{
    public:
        GlobalPosition home;
        Distance distance_to_closest_shop;

        // Constructori

        UserDistData(): home(){
        	distance_to_closest_shop = std::numeric_limits<Distance>::infinity();
        }

        UserDistData(GlobalPosition user_home, Distance closest_shop)
        {
        	home = user_home;
        	distance_to_closest_shop = closest_shop;
        }
    };

public:

	static unsigned int hashStoreGroupID(StoreGroupID id, unsigned buckets)
    {
        return id%buckets;
    }

    SinglyLinkedList<UserDistData> users;

    GlobalPosition sum;

    int nr_users;

    StoreGroupData(): sum()
    {
        nr_users = 0;
    }

    StoreGroupData(GlobalPosition user_home, Distance closest_shop): sum()
    {
        nr_users = 1;
        sum = user_home;
        users.addLast(UserDistData(user_home,closest_shop));
    }

    StoreGroupData(const StoreGroupData& copy)
    {
    	users = copy.users;
    	sum = copy.sum;
    	nr_users = copy.nr_users;
    }

    // Adauga utilizatorul cu pozitia user_home si distanta catre cel mai apropiat magazin 
    // closest_shop in grup 
    void add(GlobalPosition user_home, Distance closest_shop)
    {
    	users.addLast(UserDistData(user_home,closest_shop));
    	nr_users++;
    	sum += user_home;
    }

    // Verifica daca in urma adaugarii in grup distanta catre magazinul propus devine mai mica
    // decat distanta la cel mai apropiat magazin
    bool isGoodStage1(GlobalPosition user_home, Distance closest_shop, Distance& new_distance)
    {
    	GlobalPosition new_pos = sum;
    	new_pos += user_home;
    	new_pos /= (nr_users+1);

    	new_distance = user_home.DistanceTo(new_pos);

    	if(new_distance >= closest_shop )
    		return false;

    	
    	return true;
    }

    // Verifica daca in urma adaugarii in grup utilizatorii deja existenti raman mai aproape
    // de magazinul propos decat de cel mai apropiat magazin existent
    bool isGoodStage2(GlobalPosition user_home)
    {
        GlobalPosition new_pos = sum;
        new_pos += user_home;
        new_pos /= (nr_users+1);
        
        for(SinglyLinkedList<UserDistData>::Iterator it = users.begin() ; it.isValid() ; ++it)
        {
            if(new_pos.DistanceTo(it.getValue().home) >= it.getValue().distance_to_closest_shop)
                return false;
        }

        return true;
    }
};

#endif