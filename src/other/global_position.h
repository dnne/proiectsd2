/*
GlobalPosition - clasa folosita pentru a retine pozitii pe glob si pentru a calcula distante
*/

#ifndef GLOBAL_POSITION_H
#define GLOBAL_POSITION_H

#include <cmath>
#define RazaPamant  6371 //raza pamantului in KM
class GlobalPosition{
public:
	double latitude,longitude;

	GlobalPosition()
	{
		latitude = longitude = 0;
	}

	//calculeaza distanta dintre locatia curenta si o alta locatie pos
	double DistanceTo(GlobalPosition pos)
	{
		/*
		double x;

		x = sin(((this->latitude - pos.latitude)/2)*(M_PI/180))*
		sin(((this->latitude - pos.latitude)/2)*(M_PI/180)) + cos(this->latitude*(M_PI/180)) *
		cos(pos.latitude*(M_PI/180))*sin(((this->longitude - pos.longitude)/2)*(M_PI/180))*
		sin(((this->longitude - pos.longitude)/2)*(M_PI/180));

		double d;

		d = 2 * RazaPamant * atan(sqrt(x/(x+1)) * (M_PI/180)); 

		return d;*/

		double x = latitude-pos.latitude;
		double y = longitude-pos.longitude;

		return sqrt(x*x+y*y);
	}

	GlobalPosition& operator+=(const GlobalPosition& pos)
	{
		latitude  += pos.latitude;
		longitude += pos.longitude;
		return *this;
	}

	GlobalPosition& operator/=(int div)
	{
		latitude  /= div;
		longitude /= div;
		return *this;
	}

	GlobalPosition operator/(const int div)
	{
		GlobalPosition pos = *this;
		pos.latitude  /= div;
		pos.longitude /= div;

		return pos;
	}


};

#endif