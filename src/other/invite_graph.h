/*
InviteGraph - clasa folosita pentru a retine grupurile distince de utilizatori
*/

#ifndef INVITE_GRAPH_H
#define INVITE_GRAPH_H

#include "../generic_data_structures/avl_tree.h"
#include "../generic_data_structures/avl_set.h"
#include "../generic_data_structures/hash_map.h"
#include "../generic_data_structures/singly_linked_list.h"
#include "../typedefs.h"
#include "../limits.h"
#include "users.h"

#include "../generic_data_structures/array.h"

class InviteGraph{
private:

	// ConnectedComponent - clasa folosita pentru a retine componente conexe(nu neaparat grupuri)
	class ConnectedComponent{
	public:
		GroupID gid;
		int members;
		int visits;

		ConnectedComponent(){
			members = 1;
			visits = 0;
		}
	};

	// Numarul de grupuri
	int nr_groups;

	// Listele de vecini(invitati) pentru fiecare nod(utilizator)
	HashMap< UserID,Pair<Invites,SinglyLinkedList<UserID> > > neighbors_list;

	// Componentele conexe
	// UserID - id-ul utilizatorului radacina de la care pornesc lanturile de invitatii
	HashMap< UserID,ConnectedComponent> connected_components;

	//Topul vizitelor
	// cheia - numarul de vizite
	// Valoare - set de id-uri ale utilizatorilor care reprezinta grupurile
	// (au id-ul cel mai mic din grup)
	// Se modifica de fiecare data care apare o vizita intr-un anumit grup
	// sau cand se modifica un grup
	AvlTree<int, AvlSet<UserID> > top_visits;

	// Pointer catre structura folosita pentru memorare utilizatorilor
	UsersManager *users;

	// Functia de hash pentru id-ul utilizatorului
	unsigned int (*hashFunctionUID)(UserID, unsigned);

	// Id-ul utilizatorului care a invitat cei mai multi utilizatori
	// Initial e -1 (daca nu exista invitatii)
	// Se reface de fiecare data cand un utilizator e invitat
	UserID top_inviter;

	// Numarul maxim de invitatii folosit pentru a mentine top_inviter corect intr-un timp mai scurt
	Invites max_invites;

	// Cel mai lung sir de invitatii
	// Se actualizeaza pe masura ce se invita utilizatori
	int longest_invite_chain;

public:

	InviteGraph(UsersManager *users, unsigned int (*hashFunctionUID)(UserID, unsigned)):
		neighbors_list(USERS_BUCKETS, hashFunctionUID),
		connected_components(USERS_BUCKETS, hashFunctionUID)
	{
		nr_groups = 0;
		this->users = users;
		this->hashFunctionUID = hashFunctionUID;

		top_inviter = INVALID_USER;
		max_invites = 0;
		longest_invite_chain = 0;
	}

	// Adaugarea ca o noua componenta conexa
	// trebuie ca 'id' sa fie inserat deja in 'users'
	void addUser(UserID id)
	{
		try{
			UserData &crt_user = users->getUserData(id);
			if(crt_user.invited_by == INVALID_USER)
			{
				ConnectedComponent crt_component;
				crt_component.gid = id;
				connected_components.add(id,crt_component);
			}
		}
		catch(...){}

	}

	// Actualizare legatiri in graf pentru invitare unui utilizator
	// nu se actualizeaza si in users
	void invite(UserID inviter_id, UserID invited_id)
	{
		// Obtinerea listei de vecini
		Pair<Invites,SinglyLinkedList<UserID> >* neighbors_inviter;
		try{
			neighbors_inviter = &neighbors_list.getValue(inviter_id);
		}
		catch(...)
		{
			// Nu exista lista de vecini pentru inviter_id
			// Se creaza
			Pair<Invites,SinglyLinkedList<UserID> > neighbors_new;
			neighbors_new.t1 = 0;
			neighbors_list.add(inviter_id, neighbors_new);
			neighbors_inviter = &neighbors_list.getValue(inviter_id);
		}

		// Verificare daca invited_id a fost adaugat anterior in lista de vecini
		// a lui inviter_id
		bool found = false;
		for(SinglyLinkedList<UserID>::Iterator it=neighbors_inviter->t2.begin() ;
			it.isValid() ; ++it)
		{
			if(it.getValue() == invited_id)
			{
				// A fost adaugat anterior
				found = true;
				break;
			}
				
		}

		// Nu a fost adaugat anterior. Se adauga
		if(!found)
		{
			neighbors_inviter->t2.addLast(invited_id);
			neighbors_inviter->t1++;

			// Se actualizeaza utilizatorul cu cele mai multe invitatii daca este cazul
			if(neighbors_inviter->t1 > max_invites)
			{
				max_invites = neighbors_inviter->t1;
				top_inviter = inviter_id;
			}
			else if(neighbors_inviter->t1 == max_invites && inviter_id < top_inviter)
			{
				top_inviter = inviter_id;
			}
		}

		try{
			UserData *user_data;
			user_data = &users->getUserData(inviter_id);

			// Parcurgerea lantului de invitatii de la inviter_id catre radacina grupui
			// si actualizarea celui mai mung lant de invitatii ce porneste de la 
			// fiecare din utilizatorii parcursi in drumul spre radacina.

			int last_longest_invited_chain = 
				users->getUserData(invited_id).longest_invite_chain;

			while(user_data->invited_by != INVALID_USER)
			{
				inviter_id = user_data->invited_by;
				if(user_data->longest_invite_chain <= last_longest_invited_chain)
				{
					user_data->longest_invite_chain = last_longest_invited_chain + 1;
				}
				last_longest_invited_chain = user_data->longest_invite_chain;

				user_data = &users->getUserData(inviter_id);
			}

			// Actualizarea celui mai lung lant de invitatii din toate grupurile daca este cazul
			if(user_data->longest_invite_chain <= last_longest_invited_chain)
			{
				user_data->longest_invite_chain = last_longest_invited_chain + 1;
			}

			if(user_data->longest_invite_chain > this->longest_invite_chain)
			{
				this->longest_invite_chain = user_data->longest_invite_chain;
			}
		}
		catch(...)
		{
			return;
		}

		// Obtinerea componentelor conexe corespunzatoare celor 2 utilizatorii: 
		// inviter_id si invited_id
		ConnectedComponent& inviter_component = connected_components.getValue(inviter_id);
		ConnectedComponent& invited_component = connected_components.getValue(invited_id);

		// Daca componentele conexe au fiecare cate un singru membru (deci nu sunt grupuri)
		// prin unirea lor apare un grup
		if(inviter_component.members == 1 && invited_component.members == 1)
		{
			nr_groups++;
		}
		// Daca componentele conexe au fiecare mai mult de un membru (deci sunt grupuri)
		// prin unirea lor dispare un grup
		else if(inviter_component.members != 1 && invited_component.members != 1)
			nr_groups--;

		AvlSet<UserID> *users_set_inviter;

		// Eliminarea din topul grupurilor cu vizite (daca este cazul) a celor doua componte conexe
		if(inviter_component.members != 1)
		{
			users_set_inviter = &top_visits.getValue(inviter_component.visits);
			users_set_inviter->remove(inviter_component.gid);
			if(users_set_inviter->isEmpty())
				top_visits.remove(inviter_component.visits);
		}
		if(invited_component.members != 1)
		{
			AvlSet<UserID> *users_set_invited;
			users_set_invited = &top_visits.getValue(invited_component.visits);
			users_set_invited->remove(invited_component.gid);
			if(users_set_invited->isEmpty())
				top_visits.remove(invited_component.visits);
		}

		// Actualozarea componentei conexe corespunzatoare persoanei care a invitat
		// (preaia numarul de membrii si de vizite)
		inviter_component.visits  += invited_component.visits;
		inviter_component.members += invited_component.members;

		// Actualizarea id-ului grupui daca este cazul
		if(inviter_component.gid > invited_component.gid)
			inviter_component.gid = invited_component.gid;

		// Eliminarea componentei conexe a persoanei care fost invitata
		connected_components.remove(invited_id);

		// Introducerea in topul grupurilor cu numar mare de vizite a noului grup
		try{
			users_set_inviter = &top_visits.getValue(inviter_component.visits);
		}
		catch(...)
		{
			top_visits.set(inviter_component.visits, AvlSet<UserID>());
			users_set_inviter = &top_visits.getValue(inviter_component.visits);
		}
		users_set_inviter->set(inviter_component.gid);
	}

	void visit(UserID id)
	{
		// Obtinerea radacinii componentei conexe din care face parte utilizatorul
		try{
			UserData *user_data;
			user_data = &users->getUserData(id);
			while(user_data->invited_by != INVALID_USER)
			{
				id = user_data->invited_by;
				user_data = &users->getUserData(id);
			}
		}
		catch(...)
		{
			return;
		}

		ConnectedComponent &crt_component = connected_components.getValue(id);
		AvlSet<UserID> *users_set;

		// Actualizarea topului de vizite daca este cazul
		if(crt_component.members != 1)
		{
			users_set = &top_visits.getValue(crt_component.visits);
			users_set->remove(crt_component.gid);
			if(users_set->isEmpty())
				top_visits.remove(crt_component.visits);

			crt_component.visits++;
			try{
				users_set = &top_visits.getValue(crt_component.visits);
			}
			catch(...)
			{
				top_visits.set(crt_component.visits, AvlSet<UserID>());
				users_set = &top_visits.getValue(crt_component.visits);
			}
			users_set->set(crt_component.gid);
			return;
		}

		// Actualizarea numarului de vizite pentru componenta conexa din care 
		// face parte utilizatorul cu id-ul id
		crt_component.visits++;
	}

	int getMaxDepth()
	{
		return longest_invite_chain;
	}

	Array<int> getConnectedComponentsSizes()
	{
		int* results = nullptr;
		if(nr_groups > 0) 
			results = new int[nr_groups];
		
		int crt_component = 0;

		// Parcurgea componentelor conexe
		for(HashMap< UserID,ConnectedComponent>::Iterator it=connected_components.begin() ;
			it.isValid() ; ++it)
		{
			// Daca este grup se adauga in Array dimensiunea acestuia
			if(it.getValue().members != 1)
				results[crt_component++] = it.getValue().members;
		}

		return Array<int>(nr_groups,results);
	}

	void getVisitsPerUser(Array<std::pair<int, double>>& results)
	{
		if(nr_groups > 0)
		{
			results.resultSize = nr_groups;
			results.resultData = new std::pair<int, double>[nr_groups]; 
		}

		std::pair<int, double> pair;

		int crt_component = 0;
		// Parcurgerea componentelor conexe
		for(HashMap< UserID,ConnectedComponent>::Iterator it=connected_components.begin() ;
			it.isValid() ; ++it)
		{
			// Sunt sarite componentele conexe care au un membru (cele care nu sunt grupuri)
			if(it.getValue().members == 1)
				continue;

			// Se realizeaza raportul vizite/membrii si se adauga in Array
			// impreuna cu id-ul grupupui (cel mai mic id al user-ilor )
			pair.first = it.getValue().gid;
			pair.second = double(it.getValue().visits)/it.getValue().members;
			results.resultData[crt_component++] =std::pair<int, double>(pair);
		}
	}


	Array<int> topKVisitsGroup(int k_components)
	{
		int* results = nullptr;
		if(k_components < 1)
			return Array<int>(0,results);
		
		results = new int[k_components]; 		
		
		int limit = 0;

		AvlSet<UserID>::Iterator set_it;

		// Parcurgea topului cu vizite si adaugarea primelor K grupuri in Array (daca se poate)
		for(AvlTree<int, AvlSet<UserID> >::Iterator 
			tree_it = top_visits.end() ; tree_it.isValid() ; --tree_it)
		{
			for(set_it = tree_it.getValue().begin() ; set_it.isValid() && limit < k_components ; ++set_it)
			{
				results[limit++] = set_it.getKey();
			}
		}

		return Array<int>(limit,results);
		
	}				

	int topInviter()
	{
		return top_inviter;
	}
};





#endif
