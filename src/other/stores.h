/*
StoreData - clasa folosita pentru memorarea informatiilor despre magazine

StoreMap - wrapper peste hashtable
*/

#ifndef STORES_H
#define STORES_H


#include "../typedefs.h"
#include "../error_codes.h"
#include "../generic_data_structures/hash_map.h"
#include "../limits.h"
#include "../consts.h"
#include "global_position.h"

class StoreData{
public:
	GlobalPosition location;
	StoreData()
	{
		location.latitude=0;
		location.longitude=0;
	}

};

class StoresMap{
public:
	HashMap<StoreID, StoreData> stores;

	StoresMap(): stores(STORES_BUCKETS, hashStoreID){}

	StoreData& getStoresData(StoreID id) throw(int)
	{
		try{
			return stores.getValue(id);
		}
		catch(...){}

		throw STORE_NOT_FOUND;
	}

	void addStore(StoreID id, const StoreData& data)
	{
		stores.add(id,data);
	}

	static unsigned int hashStoreID(StoreID id, unsigned buckets)
	{
		return id%buckets;
	}

};

#endif