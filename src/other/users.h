/*
UserData - clasa folosita pentru memorarea informatiilor deste utilizatori

UserManager - wrapper peste hashtable
*/

#ifndef USERS_H
#define USERS_H

#include <limits>
#include "../typedefs.h"
#include "../error_codes.h"
#include "../generic_data_structures/hash_map.h"
#include "../limits.h"
#include "../consts.h"
#include "global_position.h"


class UserData{
public:
	UserID invited_by;
	int total_discounts;
	int buys; // numarul de vizite in care a cumparat ceva

	// cel lung lant de invitatii pornind de la utilizatorul curent
	int longest_invite_chain;

	Distance distance_to_closest_shop;
	
	GlobalPosition home;
	
	UserData()
	{
		invited_by = INVALID_USER;
		total_discounts = 0;
		buys = 0;
		longest_invite_chain = 0;
		distance_to_closest_shop = std::numeric_limits<Distance>::infinity();
	}

};

class UsersManager{
public:
	HashMap<UserID, UserData> users;
	UsersManager(): users(USERS_BUCKETS, hashUserID){}

	UserData& getUserData(UserID id) throw(int)
	{
		try{
			return users.getValue(id);
		}
		catch(...){}

		throw USER_NOT_FOUND;
	}

	void addUser(UserID id, const UserData& data)
	{
		// ANTENTIE SUPRASCRIERE
		users.add(id,data);
	}

	// Functia de hash pentru UserID
	static unsigned int hashUserID(UserID id, unsigned buckets)
	{
		return id%buckets;
	}

};

#endif