/*
Pair - clasa pereche folosita pentru hashtable (Proiect 1)
*/

#ifndef _PAIR_HEADER_
#define _PAIR_HEADER_

template <typename T1, typename T2>
class Pair
{
public:
	T1 t1;
	T2 t2;

	bool operator==(Pair<T1,T2> pair)
	{
		return (t1 == pair.t1) && (t2 == pair.t2);
	}
};

#endif