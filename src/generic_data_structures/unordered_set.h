/*
    HashTable doar cu cheie - asemanator cu std::unordered_set
*/

#ifndef __UNORDERED_SET__H
#define __UNORDERED_SET__H

#include"singly_linked_list.h"

template <typename K>
class UnorderedSet
{
private:
    unsigned buckets;
    SinglyLinkedList <K> **map;
    unsigned int (*hashFunction)(K, unsigned);


public:
    // Constructor
    UnorderedSet(unsigned int const buckets,unsigned int (*hashFunction)(K, unsigned))
    {
        this-> hashFunction = hashFunction;
        unsigned int i;
        this->buckets=buckets;
        map=new SinglyLinkedList<K>*[buckets];
        for(i = 0 ; i < buckets ; i++)
            map[i]=NULL;
    }

    // Destructor
    ~UnorderedSet()
    {
        unsigned i;
        for(i = 0 ; i < buckets ; i++)
            if(map[i] != NULL)
                delete map[i];
        delete[] map;
    }

    //functie de adaugare valori
    void add(K key)
    {
        typename SinglyLinkedList<K>::Iterator it;
        unsigned int poz = hashFunction(key,buckets);
        if(map[poz] == NULL)//daca este NULL nu avem nicio valoare adaugata
            map[poz]=new SinglyLinkedList<K>;

        for(it = map[poz]->begin() ; it.isValid() ; ++it)
        {
            if(it.getValue() == key)
            {
                return;
            }
        }
        map[poz]->addLast(key);//adaugare
        it = map[poz]->end();
    }

    void remove(K key)
    {
        typename SinglyLinkedList< K >::Iterator crt_it, next_it;
        unsigned int poz = hashFunction(key, buckets);
        if(map[poz] == NULL)
            return;

        if(map[poz]->getFirst() == key)
        {
            map[poz]->removeFirst();

            if(map[poz]->isEmpty())
            {
                delete map[poz];
                map[poz] = NULL;
            }
            return;
        }

        crt_it = next_it = map[poz]->begin();
        for(++next_it ; next_it.isValid() ; ++crt_it, ++next_it)
        {
            if(next_it.getValue() == key)
            {
                crt_it.removeAfter();
                return;
            }
        }
    }

    unsigned int getSize()
    {
        return buckets;
    }

    bool isKey(K key)
    {
        typename SinglyLinkedList<K>::Iterator it;
        unsigned int poz = hashFunction(key, buckets);

        if(map[poz] == NULL)
            return false;

        for(it = map[poz]->begin() ; it != map[poz]->end() ; ++it)
        {
            if(it.getValue()== key)
            {
                return true;
            }
        }

        return false;
    }

    class Iterator;
    friend class Iterator;

    Iterator begin()
    {
        unsigned int i;
        UnorderedSet<K>::Iterator iterator;
        iterator.set = this;

        for(i = 0 ; i < buckets ; i++)
        {
            if(map[i] != NULL)
            {
                iterator.crt_bucket = i;
                iterator.bucket_iterator = map[i]->begin();
                break;
            }
        }
        return iterator;
    }
};

template <typename K>
class UnorderedSet<K>::Iterator
{
private:
    UnorderedSet<K> *set;
    unsigned int crt_bucket;
    typename SinglyLinkedList<K>::Iterator bucket_iterator;

public:
    Iterator()
    {
        set = NULL;
        crt_bucket = 0;
        //bucket_iterator = SinglyLinkedList< Pair<K,V> >::Iterator();
    }

    K getKey() throw(int)
    {
        if(bucket_iterator.isValid() == false)
            throw int(0x1);

        return bucket_iterator.getValue();
    }

    Iterator& operator++()
    {
        ++bucket_iterator;

        if(bucket_iterator.isValid() == false)
        {

            for(crt_bucket++ ; crt_bucket < set->buckets ; crt_bucket++)
            {
                if(set->map[crt_bucket] == NULL)
                    continue;

                bucket_iterator = set->map[crt_bucket]->begin();
                break;
            }
        }
        return *this;
    }

    bool isValid()
    {
        return bucket_iterator.isValid();
    }

    bool operator==(Iterator& it)
    {
        return (bucket_iterator == it);
    }
    bool operator==(Iterator it)
    {
        return (bucket_iterator == it);
    }
    bool operator!=(Iterator& it)
    {
        return (bucket_iterator != it);
    }
    bool operator!=(Iterator it)
    {
        return (bucket_iterator != it);
    }

    friend class UnorderedSet<K>;
};
#endif
