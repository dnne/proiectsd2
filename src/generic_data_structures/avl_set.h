/*
AVL Set - Arbore binar de cautare care se auto-echilibreaza dupa inaltime in
momentul inserarii/stergerii unui nod
Info : https://en.wikipedia.org/wiki/AVL_tree

Este asemanator cu AvlTree dar are doar cheie, fara valoare

AvlSet<K>

Constructor:
	AvlSet()
Destructor:
	~AvlSet()
Methodes:

	// Adaugarea unui nod in arbore.
	// In cazul in care exista deja un nod cu cheia 'key' se va suprascrie 'value'
	void set(K key);

	// Stergerea nodului cu cheia 'removeKey' din arbore
	void remove(K removeKey);

	// Stergerea perechii <key,value> cu proprietatea ca key este cea mai mica cheie de arbore
	void removeFirst()

	// Verificare daca arborele este gol
	bool isEmpty();

	// Obtinerea unui iterator catre nodul ce are cheia cea mai mica
	Iterator begin()

	// Obtinerea unui iterator catre nodul care are cheia cea mai mare
	Iterator end()

	// Obtinerea unui itereator catre nodul ce are cheia exact searchKey
	Iterator getNodeWithKey(K searchKey) throw(int)

	// Obtinerea unui iterator catre nodul ce are cea mai mica cheie, 
	// mai mare sau egala ca 'searchKey'
	Iterator getNodeAfterKey(K searchKey)


AvlSet<K,V>::Iterator
Constructor:
	Iterator()
Methodes:
	// Verificare daca iteratorul este valid
	bool isValid()

	// Obtinerea cheii de la nodul curent
	K getKey() throw(int)

	// Stergea nodului curent. ATENTIE nu se mai poate folosi iteratorul
	void remove()

	// Trecerea la elementul urmator prin preincrementare
    Iterator& operator++()

    // Trecerea la elementul anterior prin predecrementare
    Iterator& operator--()

*/

#ifndef AVL_SET_H
#define AVL_SET_H

#ifdef DEBUG
#include <iostream>
#endif

#include <cstdlib>
#include "../error_codes.h"
#include "singly_linked_list.h"

template <typename K>
class AvlSet{
private:
	class AvlNode;
	AvlNode *pRoot;
public:
	class Iterator;

	// Constructor
	AvlSet()
	{
		pRoot = NULL;
	}

	AvlSet(const AvlSet& copy)
    {
    	pRoot = NULL;

    	if(copy.pRoot == NULL)
			return;

    	SinglyLinkedList<AvlNode*> copyList;
		AvlNode* pCrtNode;

		copyList.addLast(copy.pRoot);
		while(!copyList.isEmpty())
		{
			try
			{
				pCrtNode = copyList.removeFirst();
				if(pCrtNode->pLeft != NULL)
					copyList.addLast(pCrtNode->pLeft);
				if(pCrtNode->pRight != NULL)
					copyList.addLast(pCrtNode->pRight);
				
				this->set(pCrtNode->key);
			}
			catch(...){}
		}
    }

	// Destructor
	~AvlSet()
	{
		if(pRoot == NULL)
			return;

		SinglyLinkedList<AvlNode*> deleteList;
		AvlNode* pCrtNode;

		deleteList.addLast(pRoot);
		while(!deleteList.isEmpty())
		{
			try
			{
				pCrtNode = deleteList.removeFirst();
				if(pCrtNode->pLeft != NULL)
					deleteList.addLast(pCrtNode->pLeft);
				if(pCrtNode->pRight != NULL)
					deleteList.addLast(pCrtNode->pRight);
				delete pCrtNode;
			}
			catch(...){}
		}
	}

	// Methodes

	// Cautarea cheii searchKey in arbore 
	bool isKey(K searchKey) throw(int)
	{
		AvlNode *pCrt = pRoot;

		while(pCrt != NULL)
		{
			if(searchKey == pCrt->key)
				return true;
			else if(searchKey < pCrt->key)
				pCrt = pCrt->pLeft;
			else
				pCrt = pCrt->pRight;
		}

		return false;
	}


	// Adaugarea unui nod in arbore.
	// In cazul in care exista deja un nod cu cheia 'key' se va suprascrie 'value'
	void set(K key)
	{

		// Verificare daca exista radacina
		if(pRoot == NULL)
		{
			AvlNode *pAddNode = new AvlNode;
			pAddNode->key = key;
			pRoot = pAddNode;
			return;
		}

		// Parcurgerea arborelui
		AvlNode *pCrt = pRoot;
		while(1)
		{
			// Cheia key exista deja
			if(pCrt->key == key)
			{
				return;
			}
			// Cheia dorita este mai mica decat cheia nodului curent
			else if(key < pCrt->key)
			{
				// Se poate adauga in stanga
				if(pCrt->pLeft == NULL)
				{
					AvlNode *pAddNode = new AvlNode;
					pAddNode->key = key;
					pAddNode->pParent = pCrt;
					pCrt->pLeft = pAddNode;
					balanceTree(pAddNode);
					return;
				}
				// Nu se poate adauga in stanga
				pCrt = pCrt->pLeft;
			}
			// Cheia dorita este mai mare decat cheia nodului curent
			else
			{
				// Se poate adauga in dreapta
				if(pCrt->pRight == NULL)
				{
					AvlNode *pAddNode = new AvlNode;
					pAddNode->key = key;
					pAddNode->pParent = pCrt;
					pCrt->pRight = pAddNode;
					balanceTree(pAddNode);
					return;
				}
				// Nu se poate adauga in dreapta
				pCrt = pCrt->pRight;
			}
		}
	}

	// Stergerea nodului cu cheia 'removeKey' din arbore
	void remove(K removeKey)
	{
		AvlNode *pCrt = pRoot;

		// Gasirea nodului
		while(pCrt != NULL)
		{
			if(removeKey == pCrt->key)
				break;
			else if(removeKey < pCrt->key)
				pCrt = pCrt->pLeft;
			else
				pCrt = pCrt->pRight;
		}

		// Verificare daca nodul a fost gasit
		if(pCrt == NULL)
			return;

		removeNode(pCrt);
	}

	// Stergerea perechii <key,value> cu proprietatea ca key este cea mai mica cheie de arbore
	void removeFirst()
	{
		if(pRoot == NULL)
			return;

		AvlNode *pCrt = pRoot;
		AvlNode *pFirstBalance;

		while(pCrt->pLeft != NULL)
		{
			pCrt = pCrt->pLeft;
		}

		removeNode(pCrt);
	}

	// Verificare daca arborele este gol
	bool isEmpty()
	{
		return (pRoot == NULL);
	}

	// Obtinerea unui iterator catre nodul ce are cheia cea mai mica
	Iterator begin()
	{

		Iterator it;
		if(pRoot == NULL)
			return it;

		it.pNode = pRoot;
		it.pTree = this;

		while(it.pNode->pLeft != NULL)
		{
			it.pNode = it.pNode->pLeft;
		}
		return it;
	}

	// Obtinerea unui iterator catre nodul care are cheia cea mai mare
	Iterator end()
	{
		Iterator it;
		if(pRoot == NULL)
			return it;

		it.pNode = pRoot;
		it.pTree = this;

		while(it.pNode->pRight != NULL)
		{
			it.pNode = it.pNode->pRight;
		}
		return it;
	}

	// Obtinerea unui itereator catre nodul ce are cheia exact searchKey
	Iterator getNodeWithKey(K searchKey) throw(int)
	{
		Iterator it;
		it.pNode = pRoot;
		it.pTree = this;

		while(it.pNode != NULL)
		{
			if(searchKey == it.pNode->key)
				return it;
			else if(searchKey < it.pNode->key)
				it.pNode = it.pNode->pLeft;
			else
				it.pNode = it.pNode->pRight;
		}

		throw KEY_NOT_FOUND;
	}

	// Obtinerea unui iterator catre nodul ce are cea mai mica cheie, 
	// mai mare sau egala ca 'searchKey'
	Iterator getNodeAfterKey(K searchKey)
	{
		Iterator it;
		it.pNode = pRoot;
		it.pTree = this;

		while(it.pNode != NULL)
		{
			if(searchKey == it.pNode->key)
				return it;
			else if(searchKey < it.pNode->key)
			{
				if(it.pNode->pLeft == NULL)
					return it;
				it.pNode = it.pNode->pLeft;
			}
			else
			{
				if(it.pNode->pRight == NULL)
					break;
				it.pNode = it.pNode->pRight;
			}
		}

		while(it.pNode != NULL)
		{
			if(it.pNode->key > searchKey)
				return it;
			it.pNode = it.pNode->pParent;
		}

		return it;
	}

#ifdef DEBUG

	bool debugCheck()
	{
		if(pRoot == NULL)
			return true;

		SinglyLinkedList<AvlNode*> myList;
		AvlNode* pCrtNode;

		myList.addFirst(pRoot);
		while(!myList.isEmpty())
		{
			try
			{
				pCrtNode = myList.removeLast();

				std::cout<<pCrtNode->key<<"("<<pCrtNode->height<<")"<<"-> ";

				if(pCrtNode->pLeft != NULL)
				{
					if(pCrtNode->pLeft->pParent != pCrtNode)
						return false;
					std::cout<<pCrtNode->pLeft->key;
					myList.addFirst(pCrtNode->pLeft);
				}
				if(pCrtNode->pRight != NULL)
				{
					if(pCrtNode->pRight->pParent != pCrtNode)
						return false;
					std::cout<<';'<<pCrtNode->pRight->key;
					myList.addFirst(pCrtNode->pRight);
				}
				std::cout<<'\n';
			}
			catch(...){
				return false;
			}
		}

		return true;
	}

#endif

private:
	void removeNode(AvlNode* pCrt)
	{
		AvlNode *pFirstBalance;

		if(pCrt->pLeft == NULL)
		{
			if(pCrt->pRight != NULL)
				pCrt->pRight->pParent = pCrt->pParent;	

			if(pCrt->pParent != NULL)
			{
				if(pCrt->pParent->pLeft == pCrt)
					pCrt->pParent->pLeft = pCrt->pRight;
				else
					pCrt->pParent->pRight = pCrt->pRight;
			}
			else
			{
				pRoot = pCrt->pRight;
			}

			pFirstBalance = pCrt->pParent;
			delete pCrt;
			balanceTree(pFirstBalance);
			return;
		}
		if(pCrt->pRight == NULL) // && pCrt->pLeft != NULL
		{
			pCrt->pLeft->pParent = pCrt->pParent;

			if(pCrt->pParent != NULL)
			{
				if(pCrt->pParent->pLeft == pCrt)
					pCrt->pParent->pLeft = pCrt->pLeft;
				else
					pCrt->pParent->pRight = pCrt->pLeft;				
			}
			else
				pRoot = pCrt->pLeft;

			pFirstBalance = pCrt->pParent;
			delete pCrt;
			balanceTree(pFirstBalance);
			return;
		}

		//else (pCrt->pLeft != NULL && pCrt->pRight != NULL)
		AvlNode *pClosest = pCrt->pLeft;
		while(pClosest->pRight != NULL)
		{
			pClosest = pClosest->pRight;
		}

		// Inlocuire pCrt cu pClosest

		if(pClosest->pParent == pCrt)
			pFirstBalance = pClosest;
		else
			pFirstBalance = pClosest->pParent;

		if(pClosest->pParent->pLeft == pClosest)
			pClosest->pParent->pLeft = pClosest->pLeft;
		else
			pClosest->pParent->pRight = pClosest->pLeft;

		if(pClosest->pLeft != NULL)
			pClosest->pLeft->pParent = pClosest->pParent;

		if(pCrt->pLeft != NULL)
			pCrt->pLeft->pParent = pClosest;
		if(pCrt->pRight != NULL)
			pCrt->pRight->pParent = pClosest;
		if(pCrt->pParent != NULL)
		{
			if(pCrt->pParent->pLeft == pCrt)
				pCrt->pParent->pLeft = pClosest;
			else
				pCrt->pParent->pRight = pClosest;
		}
		else
			pRoot = pClosest;
		pClosest->pLeft = pCrt->pLeft;
		pClosest->pRight = pCrt->pRight;
		pClosest->pParent = pCrt->pParent;

		delete pCrt;

		balanceTree(pFirstBalance);
	}

	// Calcularea factorului de balanta al nodului 'nod'
	inline void calcHeight(AvlNode* nod)
	{
		nod->height = nod->pLeft != NULL  ? nod->pLeft->height : 0;

		if(nod->pRight != NULL && nod->height < nod->pRight->height )
			nod->height = nod->pRight->height;

		nod->height++;
	}

	// Echilibrarea unui subarbore de tip Left Left
	AvlNode* balanceLL(AvlNode* nod)
	{
		AvlNode* leftChild = nod->pLeft;

		nod->pLeft = leftChild->pRight;
		if(nod->pLeft != NULL)
			nod->pLeft->pParent = nod;

		leftChild->pRight = nod;
		leftChild->pParent = nod->pParent;
		if(leftChild->pParent != NULL)
		{
			if(leftChild->pParent->pLeft == nod)
				leftChild->pParent->pLeft = leftChild;
			else
				leftChild->pParent->pRight = leftChild;
		}
		else
			pRoot = leftChild;
		nod->pParent = leftChild;

		calcHeight(nod);
		calcHeight(leftChild);		

		return leftChild;
	}

	// Echilibrarea unui subarbore de tip Right Right
	AvlNode* balanceRR(AvlNode* nod)
	{
		AvlNode* rightChild = nod->pRight;

		nod->pRight = rightChild->pLeft;
		if(nod->pRight != NULL)
			nod->pRight->pParent = nod;

		rightChild->pLeft = nod;
		rightChild->pParent = nod->pParent;
		if(rightChild->pParent != NULL)
		{
			if(rightChild->pParent->pLeft == nod)
				rightChild->pParent->pLeft = rightChild;
			else
				rightChild->pParent->pRight = rightChild;
		}
		else
			pRoot = rightChild;
		nod->pParent = rightChild;

		calcHeight(nod);
		calcHeight(rightChild);

		return rightChild;
	}

	// Echilibrarea unui subarbore de tip Left Right
	inline AvlNode* balanceLR(AvlNode* nod)
	{
		nod->pLeft = balanceRR(nod->pLeft);
		return balanceLL(nod);
	}

	// Echilibrarea unui subarbore de tip Right Left
	inline AvlNode* balanceRL(AvlNode* nod)
	{
		nod->pRight = balanceLL(nod->pRight);
		return balanceRR(nod);
	}

	inline int getBalance(AvlNode *nod)
	{
		return 	(nod->pLeft  != NULL ? nod->pLeft->height  : 0)-
				(nod->pRight != NULL ? nod->pRight->height : 0); 
	}

	// Echilibrarea unui arbore pornind de la nodul 'nod'
	// (nodul cu inaltimea cea mai mica care a fost afectat)
	void balanceTree(AvlNode* nod)
	{
		while(nod != NULL)
		{
			calcHeight(nod);

			if(getBalance(nod) > 1)
			{
				if(getBalance(nod->pLeft) >= 0)
					nod = balanceLL(nod);
				else
					nod = balanceLR(nod);
			}
			else if(getBalance(nod) < -1)
			{
				if(getBalance(nod->pRight) > 0)
					nod = balanceRL(nod);
				else
					nod = balanceRR(nod);
			}

			nod = nod->pParent;
		}
	}

};

template <typename K>
class AvlSet<K>::AvlNode{
public:
	K key;
	int height;
	AvlNode *pLeft, *pRight, *pParent;

	AvlNode()
	{
		height = 1;
		pLeft = pRight = pParent = NULL;
	}
};

template <typename K>
class AvlSet<K>::Iterator{
private:
	AvlNode *pNode;
	AvlSet<K> *pTree;

public:

	friend class AvlSet;

	Iterator()
	{
		pNode = NULL;
		pTree = NULL;
	}

	// Verificare daca iteratorul este valid
	bool isValid()
	{
		return (pNode != NULL);
	}

	// Obtinerea cheii de la nodul curent
	K getKey() throw(int)
	{
		if(pNode == NULL)
			throw INVALID_ITERATOR;

		return pNode->key;
	}

	// Stergea nodului curent. ATENTIE nu se mai poate folosi iteratorul
	void remove()
	{
		if(pNode != NULL)
			pTree->removeNode(pNode);

		pNode = NULL;
		pTree = NULL;
	}

	// Trecerea la elementul urmator prin preincrementare
    Iterator& operator++()
    {
    	if(pNode == NULL)
    		return *this;

    	if(pNode->pRight != NULL)
    	{
    		pNode = pNode->pRight;
    		while(pNode->pLeft != NULL)
    		{
    			pNode = pNode->pLeft;
    		}
    	}
    	else
    	{
    		while(pNode!=NULL)
    		{
    			if(pNode->pParent == NULL)
    			{
    				pNode = NULL;
    				return *this;
    			}
    			if(pNode->pParent->pLeft == pNode)
    			{
    				pNode = pNode->pParent;
    				break;
    			}
    			pNode = pNode->pParent;
    		}
    		
    	}

    	return *this;
    }

    // Trecerea la elementul anterior prin predecrementare
    Iterator& operator--()
    {
    	if(pNode == NULL)
    		return *this;

    	if(pNode->pLeft != NULL)
    	{
    		pNode = pNode->pLeft;
    		while(pNode->pRight != NULL)
    		{
    			pNode = pNode->pRight;
    		}
    	}
    	else
    	{
    		while(pNode!=NULL)
    		{
    			if(pNode->pParent == NULL)
    			{
    				pNode = NULL;
    				return *this;
    			}
    			if(pNode->pParent->pRight == pNode)
    			{
    				pNode = pNode->pParent;
    				break;
    			}
    			pNode = pNode->pParent;
    		}
    		
    	}

    	return *this;
    }
};

#endif