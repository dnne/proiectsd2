/*
HashMap - clasa care implementeaza un Hash Table (Proiect 1)
*/

#ifndef __HASH_MAP__H
#define __HASH_MAP__H

#include"singly_linked_list.h"
#include"pair.h"

template <typename K, typename V>
class HashMap
{
private:
    unsigned buckets;
    SinglyLinkedList <Pair <K,V> > **map;
    unsigned int (*hashFunction)(K, unsigned);


public:
    //comnstructor
    HashMap(unsigned int const buckets,unsigned int (*hashFunction)(K, unsigned))
    {
        this-> hashFunction = hashFunction;
        unsigned int i;
        this->buckets=buckets;
        map=new SinglyLinkedList< Pair<K,V> >*[buckets];
        for(i = 0 ; i < buckets ; i++)
            map[i]=NULL;
    }

    //destructor
    ~HashMap()
    {
        unsigned i;
        for(i = 0 ; i < buckets ; i++)
            if(map[i] != NULL)
                delete map[i];
        delete[] map;
    }

    //functie de adaugare valori
    void add(K key,V value)
    {
        typename SinglyLinkedList< Pair<K,V> >::Iterator it;
        unsigned int poz = hashFunction(key,buckets);
        if(map[poz] == NULL)//daca este NULL nu avem nicio valoare adaugata
            map[poz]=new SinglyLinkedList<Pair <K,V> >;

        for(it = map[poz]->begin() ; it.isValid() ; ++it)
        {
            if(it.getValue().t1 == key)
            {
                it.getValue().t2=value;
                return;
            }
        }
        Pair<K,V> p;
        p.t1 = key;
        p.t2 = value;
        map[poz]->addLast(p);//adaugare
        it = map[poz]->end();
    }

    void remove(K key)
    {
        typename SinglyLinkedList< Pair<K,V> >::Iterator crt_it, next_it;
        unsigned int poz = hashFunction(key, buckets);
        if(map[poz] == NULL)
            return;

        if(map[poz]->getFirst().t1 == key)
        {
            map[poz]->removeFirst();

            if(map[poz]->isEmpty())
            {
                delete map[poz];
                map[poz] = NULL;
            }
            return;
        }

        crt_it = next_it = map[poz]->begin();
        for(++next_it ; next_it.isValid() ; ++crt_it, ++next_it)
        {
            if(next_it.getValue().t1 == key)
            {
                crt_it.removeAfter();
                return;
            }
        }
    }

    /*SinglyLinkedList< Pair<K,V> >** getMap()
    {
        return map;
    }*/

    unsigned int getSize()
    {
        return buckets;
    }

    V& getValue(K key) throw(int)
    {
        typename SinglyLinkedList< Pair<K,V> >::Iterator it;
        unsigned int poz = hashFunction(key, buckets);

        if(map[poz] == NULL)
            throw (int) 0x1;

        for(it = map[poz]->begin() ; it != map[poz]->end() ; ++it)
        {
            if(it.getValue().t1 == key)
            {
                return it.getValue().t2;
            }
        }

        throw (int) 0x1;
    }

    class Iterator;
    friend class Iterator;

    Iterator begin()
    {
        unsigned int i;
        HashMap<K,V>::Iterator iterator;
        iterator.hash_map = this;

        for(i = 0 ; i < buckets ; i++)
        {
            if(map[i] != NULL)
            {
                iterator.crt_bucket = i;
                iterator.bucket_iterator = map[i]->begin();
                break;
            }
        }
        return iterator;
    }
};

template <typename K,typename V>
class HashMap<K,V>::Iterator
{
private:
    HashMap<K,V> *hash_map;
    unsigned int crt_bucket;
    typename SinglyLinkedList< Pair<K,V> >::Iterator bucket_iterator;

public:
    Iterator()
    {
        hash_map = NULL;
        crt_bucket = 0;
        //bucket_iterator = SinglyLinkedList< Pair<K,V> >::Iterator();
    }

    V& getValue() throw(int)
    {
        if(bucket_iterator.isValid() == false)
            throw int(0x1);

        return bucket_iterator.getValue().t2;
    }

    K getKey() throw(int)
    {
        if(bucket_iterator.isValid() == false)
            throw int(0x1);

        return bucket_iterator.getValue().t1;
    }

    Iterator& operator++()
    {
        ++bucket_iterator;

        if(bucket_iterator.isValid() == false)
        {

            for(crt_bucket++ ; crt_bucket < hash_map->buckets ; crt_bucket++)
            {
                if(hash_map->map[crt_bucket] == NULL)
                    continue;

                bucket_iterator = hash_map->map[crt_bucket]->begin();
                break;
            }
        }
        return *this;
    }

    bool isValid()
    {
        return bucket_iterator.isValid();
    }

    bool operator==(Iterator& it)
    {
        return (bucket_iterator == it);
    }
    bool operator==(Iterator it)
    {
        return (bucket_iterator == it);
    }
    bool operator!=(Iterator& it)
    {
        return (bucket_iterator != it);
    }
    bool operator!=(Iterator it)
    {
        return (bucket_iterator != it);
    }

    friend class HashMap<K,V>;
};
#endif
