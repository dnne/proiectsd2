#ifndef ARRAY_H
#define ARRAY_H

template<typename T>
struct Array {
    int resultSize;
    T* resultData;

    //Constructors

    Array()
    {
        resultSize = 0;
        resultData = nullptr;
    }

    Array(int resultSize, T* resultData) :
        resultSize(resultSize), resultData(resultData) {}

    Array(const Array& copy)
    {
    	this->resultSize = copy.resultSize;
    	if(resultSize > 0 )
    		resultData = new T[resultSize];
    	else
    		resultData = nullptr;
    	for(int i=0 ; i < resultSize ; i++)
    	{
    		resultData[i] = copy.resultData[i];
    	}
    }

    // Destructor

    ~Array()
    {
    	if(resultData != nullptr)
    		delete[] resultData;
    }
};

#endif