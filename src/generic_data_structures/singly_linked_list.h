/*
    SinglyLinkedList - Lista simplu inlantuita (Proiect 1)

    Poate fi folosita pe post de coada si stiva

    Notes:
    -Daca puteti alege intre removeFirst() sau removeLast() alegeti removeFirst()
    -removeFirst() si removeLast() arunca o exceptie inca cazul in care nu 
     sunt elemente in lista. Vezi exemplu
*/

#ifndef SINGLY_LINKED_LIST_H
#define SINGLY_LINKED_LIST_H

#include <iostream>
#include "../error_codes.h"
template <typename T> class SinglyLinkedList;

template <typename T>
std::ostream& operator<<(std::ostream& out, const SinglyLinkedList<T>& list);

//o parte din scheletul clasei SinglyLinkedList
//     - http://ocw.cs.pub.ro/courses/sd-ca/articole/articol-06
template <typename T>
class SinglyLinkedList{
private:
    class Node;
    Node *pFirst, *pLast;
public: 
    // Constructor
    SinglyLinkedList()
    {
        pFirst = pLast = NULL;
    }
    // Destructor
    ~SinglyLinkedList()
    {
        SinglyLinkedList<T>::Node *pCrtNode, *pNextNode;
        if(pFirst == NULL)
            return;
        pCrtNode = pFirst;
        pNextNode = pCrtNode->next;

        while(pNextNode != NULL)
        {
            delete pCrtNode;
            pCrtNode = pNextNode;
            pNextNode = pCrtNode->next;
        }

        delete pCrtNode;
    }
 
    /* Adauga un nod cu valoarea == value la inceputul listei. */
    void addFirst(const T& value)
    {
        SinglyLinkedList<T>::Node *pNode = new SinglyLinkedList<T>::Node(value);
        if(pFirst == NULL)
        {
            pFirst = pLast = pNode;
            return;
        }
        pNode->next = pFirst;
        pFirst = pNode;
    }
 
    /* Adauga un nod cu valoarea == value la sfarsitul listei. */
    void addLast(const T& value)
    {
        SinglyLinkedList<T>::Node *pNode = new SinglyLinkedList<T>::Node(value);
        if(pFirst == NULL)
        {
            pFirst = pLast = pNode;
            return;
        }
        pLast->next = pNode;
        pLast = pNode;
    }

    /* Adauga un nod cu valoarea == value la pozitia corespunzatoarea astfel
       incat vectorul sa ramana sortat. */
    void addSorted(const T& value, int (*compareFunc)(const T&,const T&))
    {
        SinglyLinkedList<T>::Node *pNode = new SinglyLinkedList<T>::Node(value);
        if(pFirst == NULL)
        {
            pFirst = pLast = pNode;
            return;
        }

        if(compareFunc(pFirst->value,value) >= 0)
        {
            pNode->next = pFirst;
            pFirst = pNode;
            return;
        }

        SinglyLinkedList<T>::Node *pCrt;

        for(pCrt = pFirst ; pCrt->next != NULL ; pCrt = pCrt->next)
        {
            if(compareFunc(pCrt->next->value,value) >= 0)
            {
                pNode->next = pCrt->next;
                pCrt->next = pNode;
                return;
            }
        }

        pLast->next = pNode;
        pLast = pNode;

    }
 
    /* Elimina elementul de la inceputul listei si intoarce valoarea acestuia. */
    T removeFirst() throw(int)
    {

        if(pFirst == NULL)
        {
            throw (int)0x1;
        }

        T node = pFirst->value;

        SinglyLinkedList<T>::Node *pNode = pFirst;
        pFirst = pNode->next;
        if(pFirst == NULL) pLast = NULL;
        delete pNode;
        return node;
    }

    /*Consultă primul element din listă, fără a-l şterge*/
    T getFirst() throw(int)
    {
        if(pFirst == NULL)
        {
            throw EMPTY_LIST;
        }

        return pFirst->value;
    }

    T getLast() throw(int)
    {
        if(pFirst == NULL)
        {
            throw EMPTY_LIST;
        }

        return pLast->value;
    }

 
    /* Elimina elementul de la sfarsitul listei listei si intoarce valoarea acestuia. */
    T removeLast() throw(int)
    {
        if(pFirst == NULL)
        {
            throw (int)0x1;
        }

        T node = pLast->value;

        SinglyLinkedList<T>::Node *pNode = pFirst;

        if(pFirst == pLast)
        {
            delete pLast;
            pFirst = pLast= NULL;
            return node;
        }

        while(pNode->next != NULL && pNode->next != pLast){
            pNode=pNode->next;
        }

        delete pLast;
        pLast = pNode ;
        pNode->next = NULL;
        return node;
    }
 
    /* Elimina prima aparitie a elementului care are valoarea == value. */
    //T removeFirstOccurrence(T value);
 
    /* Elimina ultima aparitie a elementului care are valoarea == value. */
    //T removeLastOccurrence(T value);
 
    /* Afiseaza elementele listei pe o singura linie, separate printr-un spatiu. */
    //friend std::ostream& operator<< <>(std::ostream& out, const SinglyLinkedList<T>& list);

    SinglyLinkedList<T>& operator=(const SinglyLinkedList<T>& list)
    {
        SinglyLinkedList<T>::Node *pCrtNode, *pNextNode;

        pCrtNode = pFirst;
        while(pCrtNode != NULL)
        {
            pNextNode = pCrtNode->next;
            delete pCrtNode;
            pCrtNode = pNextNode;
        }
        pFirst = pLast = NULL;

        if(list.pFirst == NULL)
            return *this;


        const SinglyLinkedList<T>::Node *pListNode = list.pFirst;

        while(pListNode != NULL)
        {
            this->addLast(pListNode->value);
            pListNode = pListNode->next;
        }

        return *this;
    }
 
    /* Intoarce true daca lista este vida, false altfel. */
    bool isEmpty()
    {
        return (pFirst == NULL);
    }

    class Iterator;

    Iterator begin()
    {
        SinglyLinkedList<T>::Iterator iterator;
        iterator.pNode = pFirst;
        iterator.pList = this;
        return iterator;
    }

    Iterator end()
    {
        SinglyLinkedList<T>::Iterator iterator;// NULL
        return iterator;
    }
};

template <typename T>
class SinglyLinkedList<T>::Iterator
{
private:
    Node *pNode;
    SinglyLinkedList<T>* pList;

public:
    Iterator()
    {
        pNode = NULL;
        pList = NULL;
    }

    T& getValue() throw(int)
    {
        if(pNode == NULL)
            throw int(0x1);
        return pNode->value;
    }

    //Preincrementare
    Iterator& operator++()
    {
        pNode=pNode->next;
        return *this;
    }

    //Postincrementare
    Iterator operator++(int)
    {
        SinglyLinkedList<T>::Iterator newIterator = *this;
        pNode=pNode->next;
        return newIterator;
    }

    bool addAfter(const T& value)
    {
        if(pNode == NULL || pList == NULL)
            return false;

        SinglyLinkedList<T>::Node* pAddNode = new SinglyLinkedList<T>::Node(value);

        if(pNode->next == NULL)
        {
            pList->pLast = pAddNode;
        }

        pAddNode->next = pNode->next;
        pNode->next = pAddNode;

        return true;
    }
    bool removeAfter()
    {
        if(pNode == NULL || pList == NULL)
            return false;

        if(pNode->next == NULL)
        {
            return false;
        }

        SinglyLinkedList<T>::Node *pDelNode = pNode->next;
        if(pDelNode == pList->pLast)
            pList->pLast = pNode;
        pNode->next = pDelNode->next;
        delete pDelNode;

        return true;
    }

    bool operator==(Iterator& it)
    {
        return (pNode == it.pNode);
    }
    bool operator==(Iterator it)
    {
        return (pNode == it.pNode);
    }
    bool operator!=(Iterator& it)
    {
        return (pNode != it.pNode);
    }
    bool operator!=(Iterator it)
    {
        return (pNode != it.pNode);
    }

    bool isValid()
    {
        if(pNode != NULL)
            return true;

        return false;
    }

    friend class SinglyLinkedList<T>;
};

template <typename T>
class SinglyLinkedList<T>::Node {
public:
    T value;
    Node *next;

    Node (const T value)
    {
        this->value = value;
        next = NULL;
    }

    Node()
    {
        next = NULL;
    }
};

#endif
