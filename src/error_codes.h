#ifndef ERROR_CODES_H
#define ERROR_CODES_H

//static const int KEY_NOT_FOUND = 0x1;
#define KEY_NOT_FOUND 0x01
#define INVALID_ITERATOR 0x02
#define USER_NOT_FOUND 0x03
#define EMPTY_LIST 0x04
#define STORE_NOT_FOUND 0x05

#endif