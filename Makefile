.PHONY: build clean build/checker

CFLAGS=-Wall -g -Wextra -std=c++11
EXE=build/main

build: $(EXE)

$(EXE): src/main.cpp src/service.h src/consts.h src/error_codes.h src/typedefs.h src/generic_data_structures/avl_tree.h src/generic_data_structures/hash_map.h src/generic_data_structures/pair.h src/generic_data_structures/singly_linked_list.h src/other/global_position.h src/other/invite_graph.h src/other/users.h src/limits.h
	g++ $(CFLAGS) $< -o $@

build/checker:
	g++ checker/main.cpp -std=c++11 -O2 -lm -o $@

clean:
	rm $(EXE)

