#include <iostream>
#include <string>
#include <fstream>
#include "../../../src/generic_data_structures/avl_tree.h"
#include "../../../src/other/users.h"
#include <chrono>

using namespace std::chrono;

int main()
{
	high_resolution_clock::time_point t1 = high_resolution_clock::now();
	AvlTree<UserID,UserData> *tree = new AvlTree<UserID,UserData>;

	std::ifstream file("users.csv");
	if(!file) // Eroare deschidere
		return -1;

	std::string file_token=",";
	std::string line, data;
	std::size_t last_token, crt_token;

	UserData user_data;
	UserID id;

	getline(file, line); // cap de tabel
	while(getline(file, line))
	{
		last_token = 0;

		try
		{
			// Id
			crt_token = line.find(file_token);
			data = line.substr(last_token, crt_token - last_token);
			id = std::stoi(data);

			// latitude
			last_token = crt_token + 1;
			crt_token = line.find(file_token, last_token);
			data = line.substr(last_token, crt_token - last_token);
			user_data.home.latitude = std::stold(data);

			// longitudine
			last_token = crt_token + 1;
			crt_token = line.find(file_token, last_token);
			data = line.substr(last_token, crt_token - last_token);
			user_data.home.longitude = std::stold(data);

			tree->set(id,user_data);
		}
		catch(...)
		{
			continue;
		}
	}

	file.close();

	high_resolution_clock::time_point t2 = high_resolution_clock::now();
	auto duration = duration_cast<microseconds>( t2 - t1 ).count();
	std::cout<<"Adaugare terminata in "<<duration*1e-6<<'\n';
	try{
		std::cout<<tree->getValue(49434).home.latitude<<'\n';
		std::cout<<tree->getValue(7155).home.latitude<<'\n';
		std::cout<<tree->getValue(36179).home.latitude<<'\n';
	}
	catch(...){}
	
	delete tree;

	t2 = high_resolution_clock::now();
	duration = duration_cast<microseconds>( t2 - t1 ).count();
	std::cout<<"Final "<<duration*1e-6<<'\n';

	return 0;
}