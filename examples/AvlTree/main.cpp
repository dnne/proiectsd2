#include <iostream>
#include "../../src/generic_data_structures/avl_tree.h"

int main()
{
	AvlTree<int,int> *tree = new AvlTree<int,int>;

	int i;

	std::cout<<"Adaugari------------------\n";
	tree->set(7, -1);
	tree->set(6, 5);
	tree->set(8, 3);
	tree->set(1, 8);
	tree->set(5, 6);
	tree->set(0, 9);
	tree->set(4, 7);
	tree->set(9, 2);
	tree->set(10, 1);
	std::cout<<"Suprascrieri------------------\n";
	tree->set(7, 4);

	std::cout<<"Incercari getValue------------------\n";
	for(i=0 ; i<=10 ; i++)
	{
		try{
			std::cout<<i<<' '<<tree->getValue(i)<<std::endl;
		}
		catch(int error)
		{
			if(error == KEY_NOT_FOUND)
				std::cout<<"Nu s-a gasit cheia"<<i<<" dorita.\n";
		}
	}

	std::cout<<"Parcurgere SRD si modificare(inmultire cu 2)------------------\n";

	for(AvlTree<int,int>::Iterator it=tree->begin() ; it.isValid() ; ++it)
	{
		it.getValue()*=2;
		std::cout<<it.getKey()<<' '<<it.getValue()<<'\n';
	}

	std::cout<<"Parcurgere RDS si modificare(adunare cu 2)------------------\n";

	for(AvlTree<int,int>::Iterator it=tree->end() ; it.isValid() ; --it)
	{
		it.getValue()+=2;
		std::cout<<it.getKey()<<' '<<it.getValue()<<'\n';
	}

	delete tree;
	

	return 0;
}