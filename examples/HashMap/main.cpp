#include <iostream>
#include "../../src/generic_data_structures/hash_map.h"

#define SIZE_H_MAP 1<<NUMAR_BITI //valoare statica
#define NUMAR_BITI 4 //valoare configurare

unsigned int hashFunction(int id, unsigned max)
{
        return id%max;
}

int main()
{
        HashMap<int,int> X(1,hashFunction);
        HashMap<int,int>::Iterator it;
	//adaugare
        for(unsigned int i=0;i<=20;i++)
        {
                X.add(i,i+1);
        }
	

	//scoatere valuare in functie de cheie

        X.remove(0);
        X.remove(15);
        X.remove(19);
        X.remove(90);
        //iterare + afisare
        for(it=X.begin(); it.isValid() ; ++it)
                std::cout<< it.getValue()<<" ";
        try
        {
                int valu=X.getValue(16);
                std::cout<<"\nvalu = "<<valu;
        }
        catch(int exc)
        {
                std::cout<<"Error "<<exc<<". Key not found.\n";
        }
        return 0;
}
